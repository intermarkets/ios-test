//
//  CustomWebView.m
//  Drudge-SingleView
//
//  Created by Jon Wetherall on 29/07/2015.
//  Copyright (c) 2015 Jon Wetherall. All rights reserved.
//


@import Foundation;
@import UIKit;

#import "CustomWebView.h"

@interface  CustomWebView ()


@end

@implementation CustomWebView

// UIWebView delegates

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    //
    // self.webView.hidden = NO;
    
}

-(BOOL)shouldAutorotate {
    return YES;
}

@end