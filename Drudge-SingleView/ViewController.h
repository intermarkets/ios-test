//
//  ViewController.h
//  Drudge-SingleView
//
//  Created by Jon Wetherall on 05/06/2015.
//  Copyright (c) 2015 Jon Wetherall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface ViewController : UIViewController <UIWebViewDelegate>
{
    UIBarButtonItem *homeButton;
}
- (IBAction)home:(id)sender;
- (IBAction)back:(id)sender;
-(IBAction)openInSafari: (id) sender;
-(IBAction)refresh: (id) sender;



@property (nonatomic, retain) IBOutlet  UIBarButtonItem  *homeButton;

@end